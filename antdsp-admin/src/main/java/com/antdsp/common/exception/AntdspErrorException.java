package com.antdsp.common.exception;

public class AntdspErrorException extends RuntimeException {

	public AntdspErrorException() {
		super();
	}
	
	public AntdspErrorException(String message) {
		super(message);
	}
}
