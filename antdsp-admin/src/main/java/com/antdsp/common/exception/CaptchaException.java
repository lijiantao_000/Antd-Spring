package com.antdsp.common.exception;

public class CaptchaException extends RuntimeException {
	
	public CaptchaException() {
		super();
	}

	public CaptchaException(String message) {
		super(message);
	}
}
