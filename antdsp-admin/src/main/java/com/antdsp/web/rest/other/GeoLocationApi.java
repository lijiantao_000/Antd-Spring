package com.antdsp.web.rest.other;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.antdsp.common.AntdspResponse;
import com.antdsp.common.pagination.PaginationData;
import com.antdsp.dao.jpa.other.GeolocationJpa;
import com.antdsp.data.entity.other.GeoLocationCoords;

@RestController
@RequestMapping("/geolocation")
public class GeoLocationApi {
	
	@Autowired
	private GeolocationJpa geolocationJpa;
	
	@GetMapping("/list")
	public PaginationData<GeoLocationCoords> list(){
		
		return null;
	}

	@PostMapping("/upload")
	@Transactional
	public AntdspResponse upload(@RequestBody GeoLocationCoords coords) {
		
		coords.setId(null);
		
		coords.setModifier("");
		coords.setCreator("");
		coords.onPreInsert();
		
		geolocationJpa.save(coords);
		
		return AntdspResponse.success();
	}
}