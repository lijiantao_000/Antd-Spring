export default {
  'menu.home': '首页',
  'menu.system': '系统管理',
  'menu.system.user': '用户管理',
  'menu.system.menu': '菜单管理',
  'menu.system.role': '角色管理',
  'menu.account.center': '个人中心',
  'menu.account.logout': '退出登录',
  'menu.operation': '运维管理',
  'menu.operation.log': '操作日志',
  'menu.operation.article': '文章管理',
  'menu.operation.article.edit': '文章编辑',
  'menu.operation.sqlmonitor': 'SQL监控',
};
