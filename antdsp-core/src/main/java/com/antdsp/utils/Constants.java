package com.antdsp.utils;

public class Constants {
	public static final String AUTHORIZATION = "Authorization";
	
	public static final String CAPTCHA_SESSION_KEY = "CAPTCHA_SESSION_KEY";
}
