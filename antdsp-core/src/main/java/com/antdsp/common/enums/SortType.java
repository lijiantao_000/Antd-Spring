package com.antdsp.common.enums;

public enum SortType {
	
	DESC,
	ASC;

}
