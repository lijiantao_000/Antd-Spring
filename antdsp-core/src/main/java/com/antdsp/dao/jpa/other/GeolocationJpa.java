package com.antdsp.dao.jpa.other;

import org.springframework.stereotype.Repository;

import com.antdsp.dao.jpa.AntdspBaseRepository;
import com.antdsp.data.entity.other.GeoLocationCoords;

@Repository("geoloactionJpa")
public interface GeolocationJpa extends AntdspBaseRepository<GeoLocationCoords, Long> {

}