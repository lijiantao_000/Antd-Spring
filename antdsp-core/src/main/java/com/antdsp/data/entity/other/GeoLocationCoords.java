package com.antdsp.data.entity.other;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.antdsp.data.entity.AbstractEntity;

@Table(name="tb_geolocation_coords")
@Entity
public class GeoLocationCoords extends AbstractEntity{

	@Column(name="latitude", length= 32)
	private String latitude;			//纬度-十进制
	@Column(name="longitude", length= 32)
	private String longitude;		//经度-十进制
	@Column(name="accuracy", length= 32)
	private String accuracy;			//位置精度
	@Column(name="altitude", length= 32)
	private String altitude;			//海拔
	@Column(name="altitudeAccuracy", length= 32)
	private String altitudeAccuracy;	//位置的海拔精度
	@Column(name="heading", length= 32)
	private String heading;			//方向
	@Column(name="speed", length= 32)
	private String speed;			//速度
	@Column(name="timestamp")
	private long timestamp;			//响应时间日期
	@Column(name="city", length= 32)
	private String city;
	@Column(name="province", length= 32)
	private String province;
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getProvince() {
		return province;
	}
	public void setProvince(String province) {
		this.province = province;
	}
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	@Column(name="street", length= 32)
	private String street;
	
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	public String getAccuracy() {
		return accuracy;
	}
	public void setAccuracy(String accuracy) {
		this.accuracy = accuracy;
	}
	public String getAltitude() {
		return altitude;
	}
	public void setAltitude(String altitude) {
		this.altitude = altitude;
	}
	public String getAltitudeAccuracy() {
		return altitudeAccuracy;
	}
	public void setAltitudeAccuracy(String altitudeAccuracy) {
		this.altitudeAccuracy = altitudeAccuracy;
	}
	public String getHeading() {
		return heading;
	}
	public void setHeading(String heading) {
		this.heading = heading;
	}
	public String getSpeed() {
		return speed;
	}
	public void setSpeed(String speed) {
		this.speed = speed;
	}
	public long getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}
	
}